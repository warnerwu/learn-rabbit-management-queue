package main

import (
	"log"

	"strconv"

	"github.com/streadway/amqp"
)

// 我们还需要一个辅助函数来检查每个amqp调用的返回值：
func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s, %s", msg, err)
	}
}

// Fibonacci函数
func fib(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	} else {
		return fib(n-1) + fib(n-2)
	}

}

func main() {

	// --------------------------------------------------------------------------
	// 连接RabbitMQ server
	// --------------------------------------------------------------------------

	// 连接到RabbitMQ server
	connection, err := amqp.Dial("amqp://guest:guest@localhost:5672")

	// 存在错误时
	FailOnError(err, "Failed to connect to RabbitMQ")
	// 关闭连接
	defer connection.Close()

	// --------------------------------------------------------------------------
	// 创建通信协程
	// --------------------------------------------------------------------------

	// 创建协程
	ch, err := connection.Channel()
	// 存在错误时
	FailOnError(err, "Failed to open a channel")
	// 关闭协程
	defer ch.Close()

	// --------------------------------------------------------------------------
	// 消费队列消息
	// --------------------------------------------------------------------------

	// 队列声明定义
	// 请注意，我们也在这里声明队列。
	// 因为我们可能会在发布者之前启动使用者，
	// 所以我们希望在尝试使用消息之前确保队列存在。
	q, err := ch.QueueDeclare(
		"rpc_queue", // 队列名称
		false,       // 是否持久(这将关系到服务崩溃时数据还能不能恢复的问题)
		false,       // 未使用时删除
		false,       // 独家
		false,       // 没有等待
		nil,         // 参数
	)

	// 队列定义存在错误时
	FailOnError(err, "Failed to declare a queue")

	// -----------------------------------------------------------------------
	// 公平派遣
	// -----------------------------------------------------------------------
	// 您可能已经注意到调度仍然无法完全按照我们的意愿运行。
	// 例如，在有两个工人的情况下，当所有奇怪的消息都很重，甚至消息很轻时，
	// 一个工人将经常忙碌而另一个工作人员几乎不会做任何工作。
	// 好吧，RabbitMQ对此一无所知，仍然会均匀地发送消息。
	// 发生这种情况是因为RabbitMQ只是在消息进入队列时调度消息。
	// 它不会查看消费者未确认消息的数量。它只是盲目地向第n个消费者发送每个第n个消息。
	// 设置值为1的预取计数
	err = ch.Qos(
		1,     // 预取个数
		0,     // 预取大小
		false, // 全局
	)

	// 设置值为1的预取计数, 存在错误时
	FailOnError(err, "Failed to set Qos")

	// 我们即将告诉服务器从队列中传递消息。
	// 因为它会异步地向我们发送消息，
	// 所以我们将在goroutine中读取来自通道（由amqp::Consume返回）的消息。
	deliveries, err := ch.Consume(
		q.Name, // 队列名称
		"",     // 消费者
		false,  // 是否自动确认, 这个地方值得我们去注意, 这会涉及到数据正确处理问题
		false,  // 独占
		false,  // 没有本地
		false,  // 没有等待
		nil,    // 参数
	)

	// 队列消息消费时存在错误
	FailOnError(err, "Failed to register a consumer")

	// 通道初始化
	forever := make(chan bool)

	// goroutine并发处理交付数据
	go func() {
		for d := range deliveries {
			// 转换字符串到整数
			n, err := strconv.Atoi(string(d.Body))

			// 转换字符串到整数，存在错误时
			FailOnError(err, "Fail to convert body to integer")

			// Fibonacci函数参数值
			log.Printf("[.] fib(%d)", n)

			// 计算Fibonacci函数值
			response := fib(n)

			// 发送到应答回调队列
			err = ch.Publish(
				"",        // 交易所名称
				d.ReplyTo, // 路由键
				false,     // 强制
				false,     // 立即
				amqp.Publishing{
					ContentType:   "text/plain",                   // 发送到消息队列消息类型
					CorrelationId: d.CorrelationId,                // 关联ID
					Body:          []byte(strconv.Itoa(response)), // 发送到消息队列消息内容
				},
			)

			FailOnError(err, "Fail to publish a message")

			// ---------------------------------------------------------------------
			// 消息确认
			// ---------------------------------------------------------------------
			// 手动确认
			d.Ack(false)
		}
	}()

	log.Printf("[*] Awaiting RPC Request. To exit press Ctrl+C\n")

	// 输出通道内容,
	// 因为我们没有向通道内发送任何数据所有这个通道是一直等待,
	// 也就会一直阻塞这里, 所有程序不会退出, 那我们的goroutine就会一直有机会被执行
	<-forever
}
