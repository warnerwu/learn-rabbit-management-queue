package main

import (
	"log"

	"os"

	"strings"

	"math/rand"

	"strconv"

	"time"

	"github.com/streadway/amqp"
)

// 我们还需要一个辅助函数来检查每个amqp调用的返回值：
func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s, %s", msg, err)
	}
}

// 生成随机字符串
func randomString(l int) string {
	// 初始化切片
	bytes := make([]byte, l)

	for i := 0; i < l; i++ {
		bytes[i] = byte(randInt(65, 90))
	}

	return string(bytes)
}

// 生成随机整数
func randInt(min, max int) int {
	return min + rand.Intn(max-min)
}

// 处理命令行获取参数
func bodyFrom(args []string) int {
	var s string
	if (len(args) < 2) || os.Args[1] == "" {
		s = "30"
	} else {
		s = strings.Join(args[1:], " ")
	}

	n, err := strconv.Atoi(s)

	FailOnError(err, "Failed to convent arg to integer")

	return n
}

func FibonacciRPC(n int) (res int, err error) {
	// --------------------------------------------------------------------------
	// 连接RabbitMQ server
	// --------------------------------------------------------------------------

	// 连接到RabbitMQ server
	connection, err := amqp.Dial("amqp://guest:guest@localhost:5672")

	// 存在错误时
	FailOnError(err, "Failed to connect to RabbitMQ")
	// 关闭连接
	defer connection.Close()

	// --------------------------------------------------------------------------
	// 创建通信协程
	// --------------------------------------------------------------------------

	// 创建协程
	ch, err := connection.Channel()
	// 存在错误时
	FailOnError(err, "Failed to open a channel")
	// 关闭协程
	defer ch.Close()

	// --------------------------------------------------------------------------
	// 发送消息到列表
	// 要发送, 我们必须声明一个队列供我们发送, 然后我们可以发送一个消息到这个队列
	// --------------------------------------------------------------------------

	// 队列声明定义
	q, err := ch.QueueDeclare(
		"",    // 队列名称
		false, // 是否持久(这将关系到服务崩溃时数据还能不能恢复的问题)
		false, // 未使用时删除
		true,  // 独占
		false, // 没有等待
		nil,   // 参数
	)

	// 队列定义存在错误时
	FailOnError(err, "Failed to declare a queue")

	deliveries, err := ch.Consume(
		q.Name, // 队列名称
		"",     // 消费者
		true,   // 是否自动回复
		false,  // 独占
		false,  // 没有本地
		false,  // 没有等待
		nil,    // 参数
	)

	FailOnError(err, "Failed to register a consumer")

	corrId := randomString(32)

	err = ch.Publish(
		"",
		"rpc_queue",
		false,
		false,
		amqp.Publishing{
			ContentType:   "text/plain",
			CorrelationId: corrId,
			ReplyTo:       q.Name,
			Body:          []byte(strconv.Itoa(n)),
		},
	)

	FailOnError(err, "Failed to publish a message")

	for d := range deliveries {
		if corrId == d.CorrelationId {
			res, err = strconv.Atoi(string(d.Body))
			FailOnError(err, "Failed to convent body to integer")
			break
		}
	}
	return
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	n := bodyFrom(os.Args)

	log.Printf("[x] Requesting fib(%d)", n)

	res, err := FibonacciRPC(n)

	FailOnError(err, "Failed to handle RPC request")

	log.Printf("[.] Got %d", res)
}
