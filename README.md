## 连接RabbitMQ报错

> 报错

```shell
2018/11/29 16:13:46 Failed to connect to RabbitMQ, Exception (501) Reason: "EOF"
```

> 解决

```shell
➜ rabbitmqctl set_permissions -p "/" guest ".*" ".*" ".*"
Setting permissions for user "guest" in vhost "/" ...
```

## 查看队列列表

> 命令

```shell
➜ rabbitmqctl list_queues
```

> 结果

```text
➜ rabbitmqctl list_queues
Timeout: 60.0 seconds ...
Listing queues for vhost / ...
name	messages
hello	6
```

## 关于工作队列有几点需要注意

- 什么是「**消息确认**以及如何使用」
- 什么是「**持久性**以及如何使用」
- 什么是「**公平派遣**以及如何使用」

## 关于发布和订阅

### 需要注意的几个知识点:

- 什么是「**交易所**以及如何使用」
- 什么是「**临时队列**以及如何使用」
- 什么是「**绑定**以及如何使用」

**RabbitMQ中消息传递模型的核心思想是生产者永远不会将任何消息直接发送到队列。** 实际上，生产者通常甚至不知道消息是否会被传递到任何队列。

相反，生产者只能向交易所发送消息。交换是一件非常简单的事情。一方面，它接收来自生产者的消息，另一方面将它们推送到队列。交易所必须确切知道如何处理收到的消息。它应该附加到特定队列吗？它应该附加到许多队列吗？或者它应该被丢弃。其规则由交换类型定义 。

## 路由

### emit_log_direct.go 使用

```shell
// 文件使用方法
// go run publish/emit_log_direct.go warning "计算的好像不对哦, 你去瞅瞅呗, 反正我给你提醒啦"
// go run publish/emit_log_direct.go error "出错误啦"
```

### receiver_log_direct.go 使用

```shell
// 文件使用方法
// go run subscribe/receiver_log_direct.go warning error >> logs_from_rabbit.log
```

## 主题

### receiver_log_topic.go 使用

```shell
// 文件使用方法
go run subscribe/receiver_log_topic.go '#'
go run subscribe/receiver_log_topic.go "kern.*" "*.critical"
```

### emit_log_topic.go 使用

```shell
// 文件使用方法
go run publish/emit_log_topic.go "kern.critical" "A critical kernel error"
```

