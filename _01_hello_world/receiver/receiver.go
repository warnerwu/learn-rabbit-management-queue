package main

import (
	"log"

	"fmt"

	"github.com/streadway/amqp"
)

// 我们还需要一个辅助函数来检查每个amqp调用的返回值：
func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s, %s", msg, err)
	}
}

func main() {

	// --------------------------------------------------------------------------
	// 连接RabbitMQ server
	// --------------------------------------------------------------------------

	// 连接到RabbitMQ server
	connection, err := amqp.Dial("amqp://guest:guest@localhost:5672")

	// 存在错误时
	FailOnError(err, "Failed to connect to RabbitMQ")
	// 关闭连接
	defer connection.Close()

	// --------------------------------------------------------------------------
	// 创建通信协程
	// --------------------------------------------------------------------------

	// 创建协程
	ch, err := connection.Channel()
	// 存在错误时
	FailOnError(err, "Failed to open a channel")
	// 关闭协程
	defer ch.Close()

	// --------------------------------------------------------------------------
	// 消费队列消息
	// --------------------------------------------------------------------------

	// 队列声明定义
	// 请注意，我们也在这里声明队列。
	// 因为我们可能会在发布者之前启动使用者，
	// 所以我们希望在尝试使用消息之前确保队列存在。
	q, err := ch.QueueDeclare(
		"hello", // 队列名称
		false,   // 是否持久
		false,   // 未使用时删除
		false,   // 独家
		false,   // 没有等待
		nil,     // 参数
	)

	// 队列定义存在错误时
	FailOnError(err, "Failed to declare a queue")

	// 我们即将告诉服务器从队列中传递消息。
	// 因为它会异步地向我们发送消息，
	// 所以我们将在goroutine中读取来自通道（由amqp::Consume返回）的消息。
	deliveries, err := ch.Consume(
		q.Name, // 队列名称
		"",     // 消费者
		true,   // 自动
		false,  // 独家
		false,  // 没有本地
		false,  // 没有等待
		nil,    // 参数
	)

	// 队列消息消费时存在错误
	FailOnError(err, "Failed to publish a message")

	// 通道初始化
	forever := make(chan bool)

	// goroutine并发处理交付数据
	go func() {
		for d := range deliveries {
			fmt.Printf("Receiver a message %s\n", d.Body)
		}
	}()

	log.Printf("[*] Waiting for message. To exit press Ctrl+C")

	// 输出通道内容,
	// 因为我们没有向通道内发送任何数据所有这个通道是一直等待,
	// 也就会一直阻塞这里, 所有程序不会退出, 那我们的goroutine就会一直有机会被执行
	<-forever
}
