package main

import (
	"log"

	"os"

	"strings"

	"github.com/streadway/amqp"
)

// 我们还需要一个辅助函数来检查每个amqp调用的返回值：
func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s, %s", msg, err)
	}
}

// 处理命令行获取参数
func bodyFrom(args []string) string {
	var s string
	if (len(args) < 2) || os.Args[1] == "" {
		s = "hello"
	} else {
		s = strings.Join(args[1:], " ")
	}
	return s
}

func main() {
	// --------------------------------------------------------------------------
	// 连接RabbitMQ server
	// --------------------------------------------------------------------------

	// 连接到RabbitMQ server
	connection, err := amqp.Dial("amqp://guest:guest@localhost:5672")

	// 存在错误时
	FailOnError(err, "Failed to connect to RabbitMQ")
	// 关闭连接
	defer connection.Close()

	// --------------------------------------------------------------------------
	// 创建通信协程
	// --------------------------------------------------------------------------

	// 创建协程
	ch, err := connection.Channel()
	// 存在错误时
	FailOnError(err, "Failed to open a channel")
	// 关闭协程
	defer ch.Close()

	// --------------------------------------------------------------------------
	// 发送消息到列表
	// 要发送, 我们必须声明一个队列供我们发送, 然后我们可以发送一个消息到这个队列
	// --------------------------------------------------------------------------

	// 队列声明定义
	q, err := ch.QueueDeclare(
		"task_queue", // 队列名称
		true,         // 是否持久(这将关系到服务崩溃时数据还能不能恢复的问题)
		false,        // 未使用时删除
		false,        // 独家
		false,        // 没有等待
		nil,          // 参数
	)

	// 队列定义存在错误时
	FailOnError(err, "Failed to declare a queue")

	// 接收终端输入(待发送到队列的消息体)
	body := bodyFrom(os.Args)

	// 发送消息到队列
	err = ch.Publish(
		"",     // 交换
		q.Name, // 队列名称也就是(路由键)
		false,  // 强制
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent, // 交付模式: 强制
			ContentType:  "text/plain",    // 内容类型
			Body:         []byte(body),    // 发送消息byte流
		},
	)

	// 发送消息到队列时存在错误
	FailOnError(err, "Failed to publish a message")

	// 发送消息日志记录
	log.Printf("[x] sent %s", body)
}
