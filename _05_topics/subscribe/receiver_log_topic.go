package main

import (
	"log"

	"fmt"

	"os"

	"github.com/streadway/amqp"
)

// 文件使用方法
// go run subscribe/receiver_log_topic.go '#'
// go run subscribe/receiver_log_topic.go "kern.*" "*.critical"

// 我们还需要一个辅助函数来检查每个amqp调用的返回值：
func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s, %s", msg, err)
	}
}

// 处理命令行传递参数获取消息类型
func severityForm(args []string) string {
	var s string
	if (len(args) < 2) || os.Args[1] == "" {
		s = "info"
	} else {
		s = os.Args[1]
	}
	return s
}

func main() {

	// --------------------------------------------------------------------------
	// 连接RabbitMQ server
	// --------------------------------------------------------------------------

	// 连接到RabbitMQ server
	connection, err := amqp.Dial("amqp://guest:guest@localhost:5672")

	// 存在错误时
	FailOnError(err, "Failed to connect to RabbitMQ")
	// 关闭连接
	defer connection.Close()

	// --------------------------------------------------------------------------
	// 创建通信协程
	// --------------------------------------------------------------------------

	// 创建协程
	ch, err := connection.Channel()
	// 存在错误时
	FailOnError(err, "Failed to open a channel")
	// 关闭协程
	defer ch.Close()

	// --------------------------------------------------------------------------
	// 消费交易所消息
	// --------------------------------------------------------------------------

	// 交易所定义声明
	err = ch.ExchangeDeclare(
		"logs_topic", // 交易所名称
		"topic",      // 交易类型(可选值: direct, topic, headers, fanout)
		true,         // 持久化
		false,        // 未使用时自动删除
		false,        // 内部
		false,        // 没有等待
		nil,          // 参数
	)

	// 交易所定义声明, 存在错误时
	FailOnError(err, "Failed to declare an exchange")

	// 队列定义声明
	// 该方法返回时，队列实例包含RabbitMQ生成的随机队列名称。例如，它可能看起来像amq.gen-JzTY20BRgKO-HjmUJj0wLg。
	q, err := ch.QueueDeclare(
		"",    // 队列名称(在amqp客户端中，当我们将队列名称作为空字符串提供时，我们使用生成的名称创建一个非持久队列)
		false, // 是否持久(这将关系到服务崩溃时数据还能不能恢复的问题)
		false, // 未使用时删除
		true,  // 独占(当声明它的连接关闭时，队列将被删除，因为它被声明为独占。)
		false, // 没有等待
		nil,   // 参数
	)

	// 队列定义声明, 存在错误时
	FailOnError(err, "Failed to declare a queue")

	// 命令行参数不能不传递参数
	if len(os.Args) < 2 {
		log.Printf("Usage: %s [binding_key]", os.Args[0])
		os.Exit(0)
	}

	// 循环命令行参数
	for _, s := range os.Args[1:] {
		// 通过绑定键绑定一个队列到交易所
		log.Printf("Binding queue %s to exchange %s with routing key %s", q.Name, "logs_direct", s)

		// 交易所与队列绑定
		err = ch.QueueBind(
			q.Name,       // 目标队列目标
			s,            // 路由键
			"logs_topic", // 源交易所
			false,        // 没有等待
			nil,          // 绑定参数
		)

		// 设置值为1的预取计数, 存在错误时
		FailOnError(err, "Failed to bind a queue")
	}

	// 我们即将告诉服务器从交易所中传递消息。
	// 因为它会异步地向我们发送消息，
	// 所以我们将在goroutine中读取来自通道（由amqp::Consume返回）的消息。
	deliveries, err := ch.Consume(
		q.Name, // 队列名称
		"",     // 消费者
		true,   // 是否自动确认, 这个地方值得我们去注意, 这会涉及到数据正确处理问题
		false,  // 独占
		false,  // 没有本地
		false,  // 没有等待
		nil,    // 参数
	)

	// 队列消息消费时存在错误
	FailOnError(err, "Failed to register a consumer")

	// 通道初始化
	forever := make(chan bool)

	// goroutine并发处理交付数据
	go func() {
		for d := range deliveries {
			// logLeave := strings.Split(string(d.Body), "{warnerwu}")[0]
			// logBody := strings.Split(string(d.Body), "{warnerwu}")[1]
			// fmt.Printf("[x]leave(%s): %s\n", logLeave, logBody)
			fmt.Printf("[x]: %s\n", d.Body)
		}
	}()

	log.Printf("[*] Waiting for message. To exit press Ctrl+C\n")

	// 输出通道内容,
	// 因为我们没有向通道内发送任何数据所有这个通道是一直等待,
	// 也就会一直阻塞这里, 所有程序不会退出, 那我们的goroutine就会一直有机会被执行
	<-forever
}
