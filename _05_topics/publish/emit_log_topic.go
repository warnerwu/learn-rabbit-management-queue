package main

import (
	"log"

	"os"

	"strings"

	"github.com/streadway/amqp"
)

// 文件使用方法
// go run publish/emit_log_topic.go "kern.critical" "A critical kernel error"

// 我们还需要一个辅助函数来检查每个amqp调用的返回值：
func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s, %s", msg, err)
	}
}

// 处理命令行获取参数
func bodyFrom(args []string) string {
	var s string
	if (len(args) < 3) || os.Args[2] == "" {
		s = "hello"
	} else {
		s = strings.Join(args[2:], " ")
	}
	return s
}

// 处理命令行传递参数获取消息类型
func severityForm(args []string) string {
	var s string
	if (len(args) < 2) || os.Args[1] == "" {
		s = "anonymous.info"
	} else {
		s = os.Args[1]
	}
	return s
}

func main() {
	// --------------------------------------------------------------------------
	// 连接RabbitMQ server
	// --------------------------------------------------------------------------

	// 连接到RabbitMQ server
	connection, err := amqp.Dial("amqp://guest:guest@localhost:5672")

	// 存在错误时
	FailOnError(err, "Failed to connect to RabbitMQ")
	// 关闭连接
	defer connection.Close()

	// --------------------------------------------------------------------------
	// 创建通信协程
	// --------------------------------------------------------------------------

	// 创建协程
	ch, err := connection.Channel()
	// 存在错误时
	FailOnError(err, "Failed to open a channel")
	// 关闭协程
	defer ch.Close()

	// --------------------------------------------------------------------------
	// 发送消息到交易所
	// --------------------------------------------------------------------------

	// 交易所定义声明
	err = ch.ExchangeDeclare(
		"logs_topic", // 交易所名称
		"topic",      // 交易类型(可选值: direct, topic, headers, fanout)
		true,         // 持久化
		false,        // 未使用时自动删除
		false,        // 内部
		false,        // 没有等待
		nil,          // 参数
	)

	// 交易所定义声明, 存在错误时
	FailOnError(err, "Failed to declare an exchange")

	// 接收终端输入(待发送到交易所的消息体)
	body := bodyFrom(os.Args)

	// 发送消息到交易所
	err = ch.Publish(
		"logs_topic",          // 交换所名称
		severityForm(os.Args), // 路由键(为简化起见，我们假设“严重性”可以是“信息info”，“警告warning”，“错误error”之一。)
		false, // 强制
		false,
		amqp.Publishing{
			ContentType: "text/plain", // 内容类型
			Body:        []byte(body), // 发送消息byte流
		},
	)

	// 发送消息到队列时存在错误
	FailOnError(err, "Failed to publish message for exchange")

	// 发送消息日志记录
	log.Printf("[x] sent %s", body)
}
